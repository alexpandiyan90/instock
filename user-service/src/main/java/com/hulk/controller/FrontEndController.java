package com.hulk.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping("ui")
public class FrontEndController {

	@GetMapping("home")
	public String goHome() {
		return "index";
	}
}
